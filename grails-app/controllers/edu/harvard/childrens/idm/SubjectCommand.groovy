package edu.harvard.childrens.idm

/**
 * Created by Kegler on 2/11/2015.
 */
@grails.validation.Validateable
class SubjectCommand {

    String sssid // Subject-specific subject is
    Person person


    static constraints = {
        sssid(blank: false, nullable: false, unique:true)
        person( blank: false,
                nullable: false,
                validator :  { val, obj ->

                                   if(! val.dob.before(new Date())) return 'date.in.future.error'
                                   if(! (val.gender || val.gender in Gender.list())) return 'gender.not.specified.error'

                             }
        )
        

    }

}
