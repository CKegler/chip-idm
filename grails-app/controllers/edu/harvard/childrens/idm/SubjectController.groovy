package edu.harvard.childrens.idm

import grails.converters.JSON
//import org.codehaus.groovy.grails.plugins.qrcode.QRCodeRenderer

import static edu.harvard.childrens.idm.Subject.*

class SubjectController {

    static scaffold = Subject
    
    def qrCodeService
    
    def index() {
        
        params.max = 5
        
        List<Subject> subjects = Subject.createCriteria().list(max: params.max, offset: params.offset, sort: params.sort, order: params.order){
            
            if(params.lastNameSearch) { //search by last name filter
                person {
                    ilike("lastName",  params.lastNameSearch )
                }
              }
            }
        
        
       render( view: 'index', model: [subjectInstanceList: subjects, subjectInstanceCount:subjects.totalCount, params:params ])
    }


    def listJSON = {
        def sortIndex = params.sidx ?: 'sssid'
        def sortOrder = params.sord ?: 'asc'
        def maxRows = 10 //Integer.valueOf(params.rows) ?:10
        def currentPage = 1 //Integer.valueOf(params.page) ?: 1
        def rowOffset = currentPage == 1 ? 0 : (currentPage - 1) * maxRows

        List<Subject> subjects = Subject.createCriteria().list(max: maxRows, offset: rowOffset) {

            if (params.searchField ==~ 'sssid') {
                ilike('sssid', "%${params.searchString}%")
            }
            if (params.searchField ==~ 'firstName') {
                person {
                    ilike('firstName', "%${params.searchString}%")
                }
            }
            if (params.searchField ==~ 'lastName') {
                person {
                    ilike('lastName', "%${params.searchString}%")
                }
            }
            order(sortIndex, sortOrder)
        }

        def totalRows = subjects.totalCount
        def numberOfPages = Math.ceil(totalRows / maxRows)
        def results = subjects?.collect {
            [cell: [it.sssid, it.person.lastName, it.person.firstName, it.id],
             id  : it.id]
        }

        def jsonData = [rows: results, page: currentPage, records: totalRows, total: numberOfPages]
        render jsonData as JSON

    } // end of listJSON()


    def  generateQrCode() {

        def qrCodeSubject = get(params.id)

        if(!qrCodeSubject){
            flash.message = message(code: 'default.not.found.message' , args: [message(code: 'subject.label', default: 'Subject'), params.id] )
            render(view: 'index', model: [ subjectInstance: qrCodeSubject, params: params])
            return
        }
        
        // check subject for valid qrCode that hasn't expired
        
        Qrcode qrCode = Qrcode.findBySubjectAndExpirationDateGreaterThanEquals(qrCodeSubject, new Date())
        
        if(qrCode && params.targetUri){ //  show the token if the qrCode has not yet expired.
            if(params.targetUri.equalsIgnoreCase("show")){
                flash.message = message(code: 'qrcode.still.valid.message' , args: ["${qrCodeSubject.sssid}", params.id] )
                render(view: 'show', model: [ subjectInstance: qrCodeSubject, qrToken: qrCode.token , expiration: qrCode?.expirationDate?.toString() ,params: params])
                return               
                
            } else if(params.targetUri.equalsIgnoreCase("edit")) {
                flash.message = message(code: 'qrcode.still.valid.message' , args: ["${qrCodeSubject.sssid}", params.id] )
                render(view: 'edit', model: [ subjectInstance: qrCodeSubject, qrToken: qrCode.token , expiration: qrCode?.expirationDate?.toString() ,params: params])
                return
            }
        }
        
        // else generate a new qrCode for the subject
        qrCode = qrCodeService.generateSubjectQrCode(qrCodeSubject)

        if(params.targetUri.equalsIgnoreCase("show")){
            render(view: 'show', model: [ subjectInstance: qrCodeSubject, qrToken: qrCode.token , expiration: qrCode?.expirationDate?.toString() ,params: params])
            return
            
        } else if(params.targetUri.equalsIgnoreCase("edit")){

            render(view: 'edit', model: [ subjectInstance: qrCodeSubject, qrToken: qrCode.token , expiration: qrCode?.expirationDate?.toString() ,params: params])
            return
        }

        render(view: 'show', model: [ subjectInstance: qrCodeSubject, qrToken: qrCode.token , expiration: qrCode?.expirationDate?.toString() ,params: params])
    }


    def create(){

        [subjectInstance: new Subject() ]
    }
    
    def save(SubjectCommand sub){

        Subject subject = new Subject(params)
        Person person = subject.getPerson()

        if (sub.hasErrors() || !subject.validate()) { //will check for uniqueness of SSSID
            //if (sub.errors.hasFieldErrors()) {
                flash.message = "The subject data entered did not pass validation"
                render(view: 'create', model: [ cmd: sub, subjectInstance: subject, params: params])
                return
           // }
        }
        if(!person.validate()){
            flash.message = "Could not validate the person underlying the subject "
            render(view: "create", model: [params: params, subjectInstance: subject, person: person])
            return
        }
        //check if person already exists in database, based on first/last names, DOB, gender
        Person existingPerson = Person.findByLastNameAndFirstNameAndDobAndGender(
                                                         person.lastName,
                                                         person.firstName,
                                                         person.dob,
                                                         person.gender
        )
        
        if(existingPerson){
            flash.message = "This person already exists in our records. The first/last name, DOB, and Gender must be unique "
            render(view: "create", model: [params: params,  subjectInstance: subject])
            return
        }
        
        if( !person.save()){
            flash.failure = [message: "Could not save the person underlying the subject "]
            render(view: "create", model: [params: params, subjectInstance: subject, person: person])
            return
        }       
        
        subject.setPerson(person)

        if(!subject.save(flush:true)){
            flash.failure = [message: "Could not save Subject"]
            render(view: "create", model: [params: params, subjectInstance: subject])
            return
        }
        flash.message ="Subject ${subject.sssid} was successfully saved."
        redirect(action: 'index')
    }
    
    def edit(){
        
        Subject subject = Subject.get(params.id)
        
        if(!subject){
            flash.failure = [message: "Could not find the Subject with id ${params.id}"]
            render(view: "index", model: [params: params])
            return
        }


        [subjectInstance: subject]
    }
    
    
    def update(SubjectCommand sub){
        
        def updatedSubject = get(params.id)
        
        if(!updatedSubject){
            flash.message = message(code: 'default.not.found.message' , args: [message(code: 'subject.label', default: 'Subject'), params.id] )
            render(view: 'edit', model: [ subjectInstance: sub, params: params])
            return
            // }
        }


        //bind subject with new values from web form parameters
        updatedSubject.properties = params

        if (sub.hasErrors() || !( updatedSubject.validate())) {
            render(view: 'edit', model: [cmd: sub, subjectInstance: updatedSubject, person: updatedSubject.getPerson(), params: params])
            return
        }
        
        if(!updatedSubject.save(flush:true)){
            flash.failure = [message: "Could not update Subject"]
            render(view: "edit", model: [params: params,  subjectInstance: updatedSubject])
            return
        }

        redirect(action: 'index')
    }
    
    
    def updateCancel(){

        redirect(action: 'edit', model:[params: params])
    }
    
    def show(Long id){

        Subject subject = Subject.get(id)

        if(!subject){
            flash.failure = [message: "Could not find the Subject with id ${params.id}"]
            render(view: "index", model: [params: params])
            return
        }

        [subjectInstance: subject]
    }


}
