package edu.harvard.childrens.idm

/**
 * Created by Kegler on 2/11/2015.
 */
@grails.validation.Validateable
class PersonCommand {

    String firstName
    String lastName
    Date dob
    String primaryPostalCode
    Gender gender
    
    
    static constraints = {
          importFrom(Person)

    }
    
}
