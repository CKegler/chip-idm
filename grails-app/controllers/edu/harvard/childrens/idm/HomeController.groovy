package edu.harvard.childrens.idm

class HomeController {

    def index() {
        redirect(URI: "/index" )
    }
}
