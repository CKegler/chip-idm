package edu.harvard.childrens.idm

class Contactdata {

	Person person
	Date dateCreated
	Date lastUpdated
	Date archivedOn

	static hasMany = [addresses: Address,
	                  emails: Email,
	                  phones: Phone]

	static mapping = {

		person column: "PersonID"
		version false
		dateCreated column: "dateCreated"
		lastUpdated column: "lastUpdated"
		archivedOn column: "ArchivedOn"
	}

	static constraints = {
		person unique: true
		lastUpdated nullable: true
		archivedOn nullable: true
	}
}
