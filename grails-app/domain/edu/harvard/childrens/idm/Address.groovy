package edu.harvard.childrens.idm

class Address {
	Long id
	String addressType
	String street1
	String street2
	String state
	String postalCode
	String countryCode
	Date startDate
	Date endDate
	Date dateCreated
	Date lastUpdated
	Date archivedOn
	Contactdata contactdata

	static belongsTo = [Contactdata]

	static mapping = {
		id generator: 'hilo'
		version false
        addressType column: 'AddressType'
		postalCode column: "PostalCode"
		countryCode column: "CountryCode"
		startDate column: "StartDate"
		endDate column: "EndDate"
		dateCreated column: "dateCreated"
		lastUpdated column: "lastUpdated"
		archivedOn column: "ArchivedOn"
	}

	static constraints = {
		addressType nullable: true
		street1 nullable: true
		street2 nullable: true
		state nullable: true, maxSize: 100
		postalCode nullable: true, maxSize: 12
		countryCode nullable: true, maxSize: 4
		startDate nullable: true
		endDate nullable: true
		lastUpdated nullable: true
		archivedOn nullable: true
	}
}
