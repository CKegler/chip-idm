package edu.harvard.childrens.idm

class Phone {

	String phoneType
	String number
	Character isActive
	Date dateCreated
	Date lastUpdated
	Date archivedOn
	Contactdata contactdata

	static belongsTo = [Contactdata]

	static mapping = {
		id generator: 'hilo'
		version false
        isActive column: 'IsActive'
		phoneType column: "PhoneType"
		dateCreated column: "dateCreated"
		lastUpdated column: "lastUpdated"
		archivedOn column: "ArchivedOn"
	}

	static constraints = {
		phoneType nullable: true, maxSize: 20
		number maxSize: 20
		isActive nullable: true, maxSize: 1
		lastUpdated nullable: true
		archivedOn nullable: true
	}
}
