package edu.harvard.childrens.idm

class Subject {

    Long id
	String sssid // Subject-specific subject is
	String guid  // optional: globally unique identifier, useful for exchange between systems
	Integer changeNumber // for version and optimistic locking with Hibernate
	Date dateCreated
	Date lastUpdated
	Date archivedOn
	Person person

	static belongsTo = [Person]
    static hasMany = [qrcodes: Qrcode]

	static mapping = {
		id generator: 'hilo'

		changeNumber column: "ChangeNumber", version:true
		person column: 'PersonID'
		dateCreated column: "dateCreated"
		lastUpdated column: "lastUpdated"
		archivedOn column: "ArchivedOn"
	}

	static constraints = {
		sssid unique: true, maxSize: 255
		guid nullable: true, maxSize: 36
		changeNumber nullable: true
        qrcodes nullable: true
		lastUpdated nullable: true
		archivedOn nullable: true
	}


    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Subject subject = (Subject) o

        if (guid != subject.guid) return false
        if (person != subject.person) return false
        if (sssid != subject.sssid) return false

        return true
    }

    int hashCode() {
        int result
        result = sssid.hashCode()
        result = 31 * result + (guid != null ? guid.hashCode() : 0)
        result = 31 * result + person.hashCode()
        return result
    }
}
