package edu.harvard.childrens.idm

class Person {
	Long id
	String status
	String guid // optional: globally unique identifier, useful for exchange between systems
	String firstName
	String middleName
	String lastName
	Date dob
	Date dod
	String primaryPostalCode
	Integer changeNumber // for versioning and optimisitic locking with Hibernate
	Date dateCreated
	Date lastUpdated
	Date archivedOn
	Contactdata contactdata
	Demographicdata demographicdata
	Gender gender

	static hasMany = [subjects: Subject]
	static belongsTo = [Gender]

	static mapping = {
		id generator: 'hilo'

		firstName column: 'FirstName'
		middleName column: 'MiddleName'
		lastName column: 'LastName'
		primaryPostalCode column: 'PrimaryPostalCode'
        gender column: 'GenderID'
		changeNumber column: "ChangeNumber", version:true
		dateCreated column: "dateCreated"
		lastUpdated column: "lastUpdated"
		archivedOn column: "ArchivedOn"
	}

	static constraints = {
		status nullable: true, maxSize: 50
		guid nullable: true, maxSize: 36
		firstName nullable: true, maxSize: 50
		middleName nullable: true, maxSize: 50
		lastName nullable: true, maxSize: 50
        gender blank: false, nullable: false
        //gender
		dob nullable: true
		dod nullable: true
		primaryPostalCode nullable: true, size: 5..10
		changeNumber nullable: true
		lastUpdated nullable: true
		archivedOn nullable: true
		contactdata nullable: true
		demographicdata nullable: true
	}

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Person person = (Person) o

        if (dob != person.dob) return false
        if (firstName != person.firstName) return false
        if (gender != person.gender) return false
        if (guid != person.guid) return false
        if (lastName != person.lastName) return false
        if (primaryPostalCode != person.primaryPostalCode) return false
        if (subjects != person.subjects) return false

        return true
    }

    int hashCode() {
        int result
        result = (guid != null ? guid.hashCode() : 0)
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0)
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0)
        result = 31 * result + (dob != null ? dob.hashCode() : 0)
        result = 31 * result + (primaryPostalCode != null ? primaryPostalCode.hashCode() : 0)
        result = 31 * result + (gender != null ? gender.hashCode() : 0)
        return result
    }
}
