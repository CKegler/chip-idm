package edu.harvard.childrens.idm

import java.awt.Image

/**
 * Created by Kegler on 2/19/2015.
 */
class Qrcode {

    Long id
    Subject subject

    String token
    byte[] image
    //beginning timestamp of when QRCode is valid
    Date startDate
    // ending timestamp of QRCode validity
    Date expirationDate

    /*beginning timestamp of when QRCode is valid in millisecond */
    Long startDateTS
    /*ending timestamp of when QRCode is valid in milliseconds */
    Long expirationDateTS


    Date dateCreated
    Date archivedOn

    static belongsTo = [Subject]

    static mapping = {
        id generator: 'hilo'
        version false
        subject column: 'subjectID'
        token column: 'token'
        image column: 'image'
        startDate column: "startDate"
        expirationDate column: "expirationDate"
        startDateTS column: "startDateTS"
        expirationDateTS column: "expirationDateTS"
        dateCreated column: "dateCreated"
        archivedOn column: "ArchivedOn"
    }

    static constraints = {

        subject nullable: false
        dateCreated nullable: false
        archivedOn nullable: true
        startDate nullable: true
        startDate nullable: true
        startDateTS nullable: true
        expirationDate nullable: true
        expirationDateTS nullable: true
        image nullable: true
    }
}
