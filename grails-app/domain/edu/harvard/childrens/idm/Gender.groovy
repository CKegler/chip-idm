package edu.harvard.childrens.idm

class Gender {

	String name
	Date dateCreated
	Date lastUpdated
	Date archivedOn

	static hasMany = [persons: Person]

	static mapping = {
		id generator: 'hilo'
		version false
		dateCreated column: "dateCreated"
		lastUpdated column: "lastUpdated"
		archivedOn column: "ArchivedOn"
	}

	static constraints = {
		name maxSize: 50
		dateCreated nullable: true
		lastUpdated nullable: true
		archivedOn nullable: true
	}
}
