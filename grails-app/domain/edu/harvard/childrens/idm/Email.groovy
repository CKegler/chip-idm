package edu.harvard.childrens.idm

class Email {
	Long id
	String emailType
	String address
	Date dateCreated
	Date lastUpdated
	Date archivedOn
	Contactdata contactdata

	static belongsTo = [Contactdata]

	static mapping = {
		id generator: 'hilo'
		version false
		emailType column: "EmailType"
		dateCreated column: "dateCreated"
		lastUpdated column: "lastUpdated"
		archivedOn column: "ArchivedOn"
	}

	static constraints = {
        address email:true
		emailType nullable: true, maxSize: 20
		lastUpdated nullable: true
		archivedOn nullable: true
	}
}
