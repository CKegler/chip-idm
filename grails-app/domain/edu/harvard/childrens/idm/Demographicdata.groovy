package edu.harvard.childrens.idm

class Demographicdata {

	Person person
	Date dateCreated
	Date lastUpdated
	Date archivedOn

	static mapping = {

		version false
        person column: "PersonID"
        dateCreated column: "dateCreated"
        lastUpdated column: "lastUpdated"
        archivedOn column: "ArchivedOn"
	}

	static constraints = {
		person unique: true
		lastUpdated nullable: true
		archivedOn nullable: true
	}
}
