package edu.harvard.childrens.idm

import grails.transaction.Transactional
import com.nimbusds.jose.*
import com.nimbusds.jose.crypto.MACSigner
import com.nimbusds.jwt.JWTClaimsSet


@Transactional
class QrCodeService {

    String surveyAppSecret = "secret"
    Long secretTimeoutInMillis = 300000  // 5 minutes = 5 * 60 * 1000ms



    def generateSubjectQrCode(Subject subject) {

        Long currentTime = System.currentTimeMillis()
        Long expiration = currentTime + secretTimeoutInMillis
        
        JWTClaimsSet jwtClaims = new JWTClaimsSet()
        jwtClaims.setExpirationTimeClaim(expiration)
        jwtClaims.setIssuedAtClaim(currentTime)

        final String claim = UUID.randomUUID().toString()
        jwtClaims.setJWTIDClaim(claim)
        jwtClaims.setCustomClaim("subjectId", subject.id)
        
        JWSHeader header = new JWSHeader(JWSAlgorithm.HS512)
        Payload jwtBody = new Payload(jwtClaims.toJSONObject());
        JWSObject jwsObject = new JWSObject(header, jwtBody)
        
        JWSSigner signer = new MACSigner(surveyAppSecret.getBytes());
        jwsObject.sign(signer)

        String jwsParameter = jwsObject.serialize()
        
        Qrcode subjectCode = new Qrcode(
                subject: subject,
                startDate: new Date(currentTime),
              //  startDateTS: currentTime,
                expirationDate: new Date(expiration),
                //expirationDateTS: expiration,
                token: claim ,
                //image: jwsParameter,
                
        )
        subjectCode.dateCreated = new Date()
        
        if(!subjectCode.save()){
            return null
        }
        
        //def urlParams = [new BasicNameValuePair("token", jwsParameter)] as List
        //def params = URLEncodedUtils.format(urlParams, "UTF-8")
        //return new URL("${appUrl}/launch?${params}")
        
        return subjectCode
    }
}
