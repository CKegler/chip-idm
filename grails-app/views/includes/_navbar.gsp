
<g:if env="test">
%{-- PHI warning for test environment 
    <div class="navbar navbar-fixed-bottom">
        <div class="navbar-inner">
            <div class="container">
                <h2 class="text-center"><span style="color: #ff0000">WARNING: TEST USE ONLY, DO NOT STORE PHI</span></h2>
            </div>
        </div>
    </div>
    --}%
</g:if>
<div class="navbar navbar-static-top"  >
    <div class="navbar-inner">
        <div class="container">
          
            <ul class="nav">


                %{--<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_USER">--}%
                <li class="${controllerName == 'study' ? 'active': ''}"><g:link controller="home" action="index"><icon class="icon-group"></icon> ${g.message(code: 'navbar.idm')}</g:link></li>

                <li class="${controllerName in ['subject', 'enrollment'] ? 'active': ''}"><g:link controller="subject" action="index"><icon class="icon-user"></icon> ${g.message(code: 'navbar.subjects')}</g:link></li>
                  
                %{--</sec:ifAnyGranted>--}%

                <%--
                <sec:ifAllGranted roles="ROLE_ADMIN">
                    <li class="dropdown" data-animation="fade">
                        <a href="#" class="dropdown-toggle" role="button"
                           data-toggle="dropdown">${g.message(code: 'navbar.administration')}<b
                                class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li><g:link controller="survey" action="sync">Sync Surveys</g:link></li>
                            <li><g:link controller="user"
                                        target="_blank">${g.message(code: 'navbar.administration.managementconsole')}</g:link></li>
                        </ul>
                    </li>
                </sec:ifAllGranted>
                --%>
            </ul>
            
            <%--
            <sec:ifLoggedIn>
                <ul class="nav pull-right">
                    <li class="dropdown" data-animation="fade">
                        <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown">
                            <g:if test="${sec.loggedInUserInfo(field: 'givenName') && sec.loggedInUserInfo(field: 'familyName')}">
                                <sec:loggedInUserInfo field="givenName"/> <sec:loggedInUserInfo field="familyName"/> (<sec:username/>)
                            </g:if>
                            <g:else>
                                <sec:username/>
                            </g:else>

                            <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li><g:link controller="logout">${g.message(code: 'navbar.logout')}</g:link></li>
                        </ul>
                    </li>
                </ul>
            </sec:ifLoggedIn>
            
            --%>
            
        </div>
    </div>
</div>




