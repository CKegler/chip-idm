<%@ page import="edu.harvard.childrens.idm.Gender; edu.harvard.childrens.idm.Subject" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'subject.label', default: 'Subject')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require module="jquery"/>
    <r:layoutResources/>

    <script language="JavaScript">
        javascript:window.history.forward(1);
    </script>
    
</head>

<body>

<r:layoutResources/>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-subject" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list subject">

    </ol>

    
    <g:form url="[resource: subjectInstance, action: 'delete']" method="DELETE">

        <g:if test="!${qrToken}">
        <g:link  action="generateQrCode" id="${subjectInstance.id}" params="[targetUri: 'show']" >Generate QR Code</g:link>
        
        </g:if>
        <g:if test="${expiration}">
            <qrcode:text >
               ${qrToken}
            </qrcode:text>
        <br/>     
           The QR Code expires on : ${expiration}
        </g:if>
        
        <table>
            <th>&nbsp;</th>
            <th>Subject Record (Read-Only)</th>


            <tr>
                <th>Study-specific Subject ID: </th>
                <td><g:textField name="sssid" value="${subjectInstance.sssid}" readonly="true" tabindex="1"/></td>


            </tr>
            <tr>
                <th>First Name: </th>
                <td><g:textField name="person.firstName" value="${subjectInstance.person.firstName}" readonly="true" tabindex="2"/></td>


            </tr>
            <tr>
                <th>Last Name:</th>
                <td><g:textField name="person.lastName"  value="${subjectInstance.person.lastName}" readonly="true" tabindex="3"/></td>

            </tr>
            <tr>
                <th>Date of Birth:</th>
                <td><g:datePicker name="person.dob"
                                  value="${subjectInstance.person.dob}"
                                  precision="day"
                                  years="${Calendar.getInstance().get(Calendar.YEAR)..1981}" 
                                  disabled="true"
                                  tabindex="4"/></td>

            </tr>
            <tr>
                <th>Gender:</th>
                <td>
                    <g:select name='person.gender'
                              value="${subjectInstance.person.gender.id}"
                              noSelection="${['null':'Select Gender']}"
                              from='${Gender.list()}'
                              optionKey="id" optionValue="name"
                              disabled="true"
                              tabindex="5">
                    </g:select>
                </td>

            </tr>
            <tr>
                <th>Postal Code:</th>
                <td><g:textField name="person.primaryPostalCode" value="${subjectInstance.person.primaryPostalCode}" readonly="true" tabindex="6"/></td>

            </tr>

        </table>
        
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${subjectInstance}"><g:message code="default.button.edit.label"
                                                                                        default="Edit"/></g:link>
            %{--<g:actionSubmit class="delete" action="delete"--}%
                            %{--value="${message(code: 'default.button.delete.label', default: 'Delete')}"--}%
                            %{--onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>--}%
        </fieldset>
    </g:form>
</div>



</body>
</html>
