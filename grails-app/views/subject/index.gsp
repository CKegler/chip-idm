<%@ page import="edu.harvard.childrens.idm.Subject" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'subject.label', default: 'Subject')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-subject" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-subject" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:form>
       
    <table>
        <thead>
        <tr>
            <td colspan="4">
                Last Name:
                <g:textField name="lastNameSearch" >Enter a last name</g:textField>
                <g:actionSubmit value="${message(code: 'default.button.filter.label', default: 'Filter')}" action="index"/>

            </td>
        </tr>
        <tr>

            %{--<th>QR Code</th>--}%
            <th>EDIT</th>
            <g:sortableColumn property="sssid" title="SubjectID" />
            <g:sortableColumn property="person.lastName" title="Last Name" />
            <g:sortableColumn property="person.firstName" title="First Name" />

        </tr>
        </thead>
        <tbody>

        <g:each in="${subjectInstanceList}" status="i" var="subjectInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                %{--<td>Generate</td>--}%
                <td>
                     <g:link action="edit" id="${subjectInstance.id}" >EDIT</g:link>
                </td>
                <td><g:link action="show" id="${subjectInstance.id}" >${fieldValue(bean:subjectInstance, field:'sssid')}</g:link></td>
                <td>${fieldValue(bean:subjectInstance, field:'person.lastName')}</td>
                <td>${fieldValue(bean:subjectInstance, field:'person.firstName')}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    </g:form>
        
    <div class="pagination">
        <g:paginate total="${subjectInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
