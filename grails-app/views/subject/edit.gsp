<%@ page import="edu.harvard.childrens.idm.Gender; edu.harvard.childrens.idm.Subject" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'label', default: 'Subject')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#edit-subject" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="edit-subject" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:if test="${flash.error}">
        <div class="errors" role="status">${flash.error}</div>
    </g:if>

    <g:hasErrors bean="${cmd}">
        <ul class="errors" role="alert">
            <g:eachError bean="${cmd}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${subjectInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${subjectInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${person}">
        <ul class="errors" role="alert">
            <g:eachError bean="${person}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>


    
    <g:form url="[resource: subjectInstance, action: 'update']" method="PUT">
        <g:hiddenField name="version" value="${subjectInstance?.version}"/>

        <g:if test="!${qrToken}">
            <g:link  action="generateQrCode" id="${subjectInstance.id}" params="[targetUri: 'edit']" >Generate QR Code</g:link>

        </g:if>
        <g:if test="${expiration}">
            <qrcode:text >
                ${qrToken}
            </qrcode:text>
            <br/>
            The QR Code expires on : ${expiration}
        </g:if>

        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>

        <table>
            <th width="20%">&nbsp;</th>
            <th>Old Subject Record (Read-only)</th>
            <th>New Subject Record</th>


            <tr>
                <th>Study-specific Subject ID: </th>
                <td><g:textField name="pre.sssid"  value="${subjectInstance?.sssid}" readonly="true"/></td>
                <td><g:textField name="sssid" required="true" value="${subjectInstance?.sssid}" max="255" tabindex="1"/></td>


            </tr>
            <tr>
                <th>First Name: </th>
                <td><g:textField name="pre.person.firstName" value="${subjectInstance?.person?.firstName}" readonly="true"/></td>
                <td><g:textField name="person.firstName" required="true"  value="${subjectInstance?.person?.firstName}" max="50" tabindex="2"/></td>


            </tr>
            <tr>
                <th>Last Name:</th>
                <td><g:textField name="pre.person.lastName" value="${subjectInstance?.person?.lastName}" readonly="true"/></td>
                <td><g:textField name="person.lastName" required="true" value="${subjectInstance?.person?.lastName}" max="255" tabindex="3"/></td>

            </tr>
            <tr>
                <th>Date of Birth:</th>
                <td><g:datePicker name="pre.person.dob"
                                  value="${subjectInstance?.person?.dob}"
                                  precision="day" disabled="true"
                                  /></td>
                <td><g:datePicker name="person.dob"
                                  value="${subjectInstance?.person?.dob}"
                                  precision="day"
                                  years="${Calendar.getInstance().get(Calendar.YEAR)..1981}"
                                  tabindex="4"/></td>

            </tr>
            <tr>
                <th>Gender:</th>
                <td>
                    <g:select name='pre.person.gender' disabled="true"
                              value="${subjectInstance?.person?.gender?.id}"
                              noSelection="${['null':'Select Gender']}"
                              from='${Gender.list()}' 
                              optionKey="id" optionValue="name">

                    </g:select>
                </td>

                <td>
                    <g:select name='person.gender'
                              value="${subjectInstance?.person?.gender?.id}"
                              noSelection="${['null':'Select Gender']}"
                              from='${Gender.list()}'
                              optionKey="id" optionValue="name">
                    </g:select>
                </td>

            </tr>
            <tr>
                <th>Postal Code:</th>
                <td><g:textField name="pre.person.primaryPostalCode" value="${subjectInstance?.person?.primaryPostalCode}" readonly="true"/></td>
                <td><g:textField name="person.primaryPostalCode" value="${subjectInstance?.person?.primaryPostalCode}"  min="5" maxlength="10" required="true" tabindex="5"/></td>

            </tr>

        </table>

        <fieldset class="buttons">
            <g:actionSubmit class="save" action="update"
                            value="${message(code: 'default.button.update.label', default: 'Update')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>

            <g:link class="edit" action="edit" resource="${subjectInstance}"><g:message code="default.button.cancel.label"
                                                                                        default="Cancel"/></g:link>
        </fieldset>
    </g:form>
</div>
</body>
</html>
