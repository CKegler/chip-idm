<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="layout" content="main">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>List Subjects Grid</title>

    <r:require module="jquery"/>




    %{--<link rel="stylesheet" type="text/css" media="screen" href="css/ui-lightness/jquery-ui.css" />--}%
    %{--<link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />--}%

    %{--<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>--}%
    %{--<script src="js/i18n/grid.locale-en.js" type="text/javascript"></script>--}%
    %{--<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>--}%


    <jq:resources />
    <jqui:resources />
    <jqgrid:resources />


    <script type="text/javascript">
        $(document).ready(function() {
            $("#subject").jqGrid({
                url:"${createLink(action: 'listJSON')}",

                colNames: ["Subject ID", "Last Name", "First Name", "id"],
                colModel: [
                    {  name:'sssid', editable: true},
                    {  name:'lastName', editable: true},
                    {  name:'firstName', editable: true },-
                    {  name:'id', hidden: true}
                ],
                mtype: "post",
                sortname: "lastName",
                pager: "#pager",
                caption : "Subject List",
                height : 300,
                autowidth: true,
                scrollOffset : 0,
                viewrecords : true,
                gridview : true,
                showPager : true,
                datatype : "json"
            });
        });
    </script>

    <g:layoutTitle/>
    <r:layoutResources/>
</head>
<body>


<g:layoutBody/>
<r:layoutResources/>

<jqgrid:wrapper id="subject" />



</body>
</html>