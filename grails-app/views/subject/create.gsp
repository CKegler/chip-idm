<%@ page import="edu.harvard.childrens.idm.Gender" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'subject.label', default: 'Subject')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-subject" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-subject" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:hasErrors bean="${cmd}">
        <ul class="errors" role="alert">
            <g:eachError bean="${cmd}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    
    <g:hasErrors bean="${subjectInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${subjectInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <g:hasErrors bean="${person}">
        <ul class="errors" role="alert">
            <g:eachError bean="${person}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <g:form  action="save"   method="post" >
        <fieldset class="form">
            <g:render template="form"/>


        <table>
            <th>&nbsp;</th>
            <th>Enter Subject Record</th>


            <tr>
                <th>Study-specific Subject ID: </th>
                <td><g:textField name="sssid" value="${subjectInstance?.sssid}" max="255" required="true" tabindex="1"/></td>


            </tr>
            <tr>
                <th>First Name: </th>
                <td><g:textField name="person.firstName" value="${subjectInstance?.person?.firstName}" max="50" required="true" tabindex="2"/></td>


            </tr>
            <tr>
                <th>Last Name:</th>
                <td><g:textField name="person.lastName" value="${subjectInstance?.person?.lastName}" max="255" required="true" tabindex="3"/></td>

            </tr>
            <tr>
                <th>Date of Birth:</th>
                <td><g:datePicker name="person.dob"
                                  value="${subjectInstance?.person?.dob?: new Date()}"
                                  precision="day"
                                  years="${Calendar.getInstance().get(Calendar.YEAR)..1981}"
                                  tabindex="4"
                                  /></td>

            </tr>
            <tr>
                <th>Gender:</th>
                <td>
                    <g:select name='person.gender'
                              noSelection="${['null':'Select Gender']}"
                              from='${Gender.list()}'
                              optionKey="id" optionValue="name"
                              value="${subjectInstance?.person?.gender?.id}"
                              tabindex="5">
                    </g:select>
                </td>

            </tr>
            <tr>
                <th>Postal Code:</th>
                <td><g:textField name="person.primaryPostalCode"  value="${subjectInstance?.person?.primaryPostalCode}"
                                 required="true" min="5" maxlength="10" tabindex="6"/></td>

            </tr>

        </table>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="save"
                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
            <g:link class="list" action="index" resource="${subjectInstance}"><g:message code="default.button.cancel.label"
                                                                                        default="Cancel"/></g:link>
        </fieldset>
    </g:form>
</div>
</body>
</html>
