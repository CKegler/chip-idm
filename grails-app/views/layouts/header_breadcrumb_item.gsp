
<li class="${pageProperty(name:'activeItem') == 'true' ? 'active' : ''}">
    <g:layoutBody/>
    <g:if test="${pageProperty(name:'activeItem') == 'false' }">
        <span class="divider">/</span>
    </g:if>
</li>


