<%--
  Created by IntelliJ IDEA.
  User: Kegler
  Date: 2/5/2015
  Time: 4:15 PM
--%>

<div class="breadcrumb-header">
    <h3>
        <ul class="breadcrumb">
            <li><g:link controller="home"><i class="icon-home"></i></g:link> <span class="divider">/</span></li>
            <g:if test="${controllerName in ['patient', 'enrollment']}">
                <g:if test="${params.action == 'list'}">
                    <li><g:link controller="patient" action="list">Subjects</g:link></li>
                </g:if>
                <g:else>
                    <li><g:link controller="patient" action="list">Subjects</g:link> <span class="divider">/</span></li>
                </g:else>

            </g:if>
            <g:elseif test="${controllerName == 'site'}">
                <li><g:link controller="site" action="list">Sites</g:link> <span class="divider">/</span></li>
            </g:elseif>
            <g:elseif test="${controllerName == 'study'}">
                <li><g:link controller="study" action="list">Studies</g:link> <span class="divider">/</span></li>
            </g:elseif>
            <g:elseif test="${controllerName == 'survey'}">
                <li><g:link controller="survey" action="list">Surveys</g:link> <span class="divider">/</span></li>
            </g:elseif>

            <g:layoutBody/>
        </ul>
    </h3>
</div>

S