-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema idm
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema idm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `idm` DEFAULT CHARACTER SET utf8 ;
USE `idm` ;

-- -----------------------------------------------------
-- Table `idm`.`gender`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`gender` ;

CREATE TABLE IF NOT EXISTS `idm`.`gender` (
  `ID` INT(11) NOT NULL,
  `Name` VARCHAR(50) NOT NULL,
  `dateCreated` DATETIME NULL DEFAULT NULL,
  `lastUpdated` DATETIME NULL DEFAULT NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `idm`.`person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`person` ;

CREATE TABLE IF NOT EXISTS `idm`.`person` (
  `ID` INT(11) NOT NULL,
  `Status` VARCHAR(50) NULL DEFAULT NULL,
  `GUID` CHAR(36) NULL DEFAULT NULL,
  `FirstName` VARCHAR(50) NULL DEFAULT NULL,
  `MiddleName` VARCHAR(50) NULL DEFAULT NULL,
  `LastName` VARCHAR(255) NULL DEFAULT NULL,
  `DOB` DATETIME NULL DEFAULT NULL,
  `DOD` DATETIME NULL DEFAULT NULL,
  `GenderID` INT(11) NULL DEFAULT NULL,
  `PrimaryPostalCode` VARCHAR(10) NULL DEFAULT NULL,
  `ChangeNumber` INT(11) NULL DEFAULT '0',
  `dateCreated` DATETIME NOT NULL,
  `lastUpdated` DATETIME NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `Person_Gender_FK` (`GenderID` ASC),
  CONSTRAINT `Person_Gender_FK`
    FOREIGN KEY (`GenderID`)
    REFERENCES `idm`.`gender` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `idm`.`contactdata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`contactdata` ;

CREATE TABLE IF NOT EXISTS `idm`.`contactdata` (
  `PersonID` INT(11) NOT NULL,
  `dateCreated` DATETIME NOT NULL,
  `lastUpdated` DATETIME NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`PersonID`),
  CONSTRAINT `ContactData_Person_FK`
    FOREIGN KEY (`PersonID`)
    REFERENCES `idm`.`person` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `idm`.`address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`address` ;

CREATE TABLE IF NOT EXISTS `idm`.`address` (
  `ID` INT(11) NOT NULL,
  `PersonID` INT(11) NOT NULL,
  `AddressType` VARCHAR(255) NULL DEFAULT NULL,
  `Street1` VARCHAR(255) NULL DEFAULT NULL,
  `Street2` VARCHAR(255) NULL DEFAULT NULL,
  `State` VARCHAR(100) NULL DEFAULT NULL,
  `PostalCode` VARCHAR(12) NULL DEFAULT NULL,
  `CountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `StartDate` DATETIME NULL DEFAULT NULL,
  `EndDate` DATETIME NULL DEFAULT NULL,
  `dateCreated` DATETIME NOT NULL,
  `lastUpdated` DATETIME NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `Address_ContactData_FK` (`PersonID` ASC),
  CONSTRAINT `Address_ContactData_FK`
    FOREIGN KEY (`PersonID`)
    REFERENCES `idm`.`contactdata` (`PersonID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `idm`.`demographicdata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`demographicdata` ;

CREATE TABLE IF NOT EXISTS `idm`.`demographicdata` (
  `PersonID` INT(11) NOT NULL,
  `dateCreated` DATETIME NOT NULL,
  `lastUpdated` DATETIME NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`PersonID`),
  CONSTRAINT `DemographicData_Person_FK`
    FOREIGN KEY (`PersonID`)
    REFERENCES `idm`.`person` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `idm`.`email`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`email` ;

CREATE TABLE IF NOT EXISTS `idm`.`email` (
  `ID` INT(11) NOT NULL,
  `PersonID` INT(11) NOT NULL,
  `EmailType` VARCHAR(20) NULL DEFAULT NULL,
  `Address` VARCHAR(255) NOT NULL,
  `dateCreated` DATETIME NOT NULL,
  `lastUpdated` DATETIME NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `Email_ContactData_FK` (`PersonID` ASC),
  CONSTRAINT `Email_ContactData_FK`
    FOREIGN KEY (`PersonID`)
    REFERENCES `idm`.`contactdata` (`PersonID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `idm`.`phone`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`phone` ;

CREATE TABLE IF NOT EXISTS `idm`.`phone` (
  `ID` INT(11) NOT NULL,
  `PersonID` INT(11) NOT NULL,
  `PhoneType` VARCHAR(20) NULL DEFAULT NULL,
  `Number` VARCHAR(20) NOT NULL,
  `IsActive` CHAR(1) NULL DEFAULT NULL,
  `dateCreated` DATETIME NOT NULL,
  `lastUpdated` DATETIME NULL,
  `ArchivecOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `Phone_ContactData_FK` (`PersonID` ASC),
  CONSTRAINT `Phone_ContactData_FK`
    FOREIGN KEY (`PersonID`)
    REFERENCES `idm`.`contactdata` (`PersonID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `idm`.`subject`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`subject` ;

CREATE TABLE IF NOT EXISTS `idm`.`subject` (
  `ID` INT(11) NOT NULL,
  `PersonID` INT(11) NOT NULL,
  `SSSID` VARCHAR(255) NOT NULL,
  `GUID` CHAR(36) NULL DEFAULT NULL,
  `ChangeNumber` INT(11) NULL DEFAULT '0',
  `dateCreated` DATETIME NOT NULL,
  `lastUpdated` DATETIME NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `SubjectId__UQ` (`SSSID` ASC),
  INDEX `Subject_Person_FK` (`PersonID` ASC),
  CONSTRAINT `Subject_Person_FK`
    FOREIGN KEY (`PersonID`)
    REFERENCES `idm`.`person` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `idm`.`qrcode`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `idm`.`qrcode` ;

CREATE TABLE IF NOT EXISTS `idm`.`qrcode` (
  `ID` INT(11) NOT NULL,
  `subjectID` INT(11) NULL DEFAULT NULL,
  `startDate` DATETIME NULL DEFAULT NULL,
  `startDateTS` BIGINT NULL DEFAULT NULL,
  `expirationDate` BIGINT NULL DEFAULT NULL,
  `expirationDateTS` MEDIUMTEXT NULL DEFAULT NULL,
  `token` BLOB NULL DEFAULT NULL,
  `image` MEDIUMBLOB NULL DEFAULT NULL,
  `dateCreated` DATETIME NOT NULL,
  `ArchivedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `qrcode_subject_idx` (`subjectID` ASC),
  CONSTRAINT `subject_qrcode_fk`
    FOREIGN KEY (`subjectID`)
    REFERENCES `idm`.`subject` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8




SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
