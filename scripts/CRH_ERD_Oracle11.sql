-- Generated by Oracle SQL Developer Data Modeler 4.0.3.853
--   at:        2015-02-19 16:44:42 EST
--   site:      Oracle Database 11g
--   type:      Oracle Database 11g




DROP
  TABLE Address CASCADE CONSTRAINTS ;

DROP
  TABLE ContactData CASCADE CONSTRAINTS ;

DROP
  TABLE DemographicData CASCADE CONSTRAINTS ;

DROP
  TABLE Email CASCADE CONSTRAINTS ;

DROP
  TABLE Gender CASCADE CONSTRAINTS ;

DROP
  TABLE Person CASCADE CONSTRAINTS ;

DROP
  TABLE Phone CASCADE CONSTRAINTS ;

DROP
  TABLE Qrcode CASCADE CONSTRAINTS ;

DROP
  TABLE Subject CASCADE CONSTRAINTS ;

CREATE
  TABLE Address
  (
    ID          INTEGER NOT NULL ,
    PersonID    INTEGER NOT NULL ,
    AddressType VARCHAR2 (255 CHAR) ,
    Street1     VARCHAR2 (255 CHAR) ,
    Street2     VARCHAR2 (255 CHAR) ,
    State       VARCHAR2 (100 CHAR) ,
    PostalCode  VARCHAR2 (12 CHAR) ,
    CountryCode VARCHAR2 (4 CHAR) ,
    StartDate   DATE ,
    EndDate     DATE ,
    dateCreated DATE NOT NULL ,
    lastUpdated DATE ,
    ArchivedOn  DATE
  ) ;
ALTER TABLE Address ADD CONSTRAINT Address_PK PRIMARY KEY ( ID ) ;

CREATE
  TABLE ContactData
  (
    PersonID    INTEGER NOT NULL ,
    dateCreated DATE NOT NULL ,
    lastUpdated DATE ,
    ArchivedOn  DATE
  ) ;
ALTER TABLE ContactData ADD CONSTRAINT ContactData_PK PRIMARY KEY ( PersonID )
;

CREATE
  TABLE DemographicData
  (
    PersonID    INTEGER NOT NULL ,
    dateCreated DATE NOT NULL ,
    lastUpdated DATE ,
    ArchivedOn  DATE
  ) ;
ALTER TABLE DemographicData ADD CONSTRAINT DemographicData_PK PRIMARY KEY (
PersonID ) ;

CREATE
  TABLE Email
  (
    ID          INTEGER NOT NULL ,
    PersonID    INTEGER NOT NULL ,
    EmailType   VARCHAR2 (20 CHAR) ,
    Address     VARCHAR2 (255 CHAR) NOT NULL ,
    dateCreated DATE NOT NULL ,
    lastUpdated DATE ,
    ArchivedOn  DATE
  ) ;
ALTER TABLE Email ADD CONSTRAINT Phone_PK PRIMARY KEY ( ID ) ;

CREATE
  TABLE Gender
  (
    ID          INTEGER NOT NULL ,
    Name        VARCHAR2 (50 CHAR) NOT NULL ,
    dateCreated DATE ,
    lastUpdated DATE ,
    ArchivedOn  DATE
  ) ;
ALTER TABLE Gender ADD CONSTRAINT Gender_PK PRIMARY KEY ( ID ) ;

CREATE
  TABLE Person
  (
    ID                INTEGER NOT NULL ,
    Status            VARCHAR2 (50 CHAR) ,
    GUID              CHAR (36 CHAR) ,
    FirstName         VARCHAR2 (50 CHAR) ,
    MiddleName        VARCHAR2 (50 CHAR) ,
    LastName          VARCHAR2 (255 CHAR) ,
    DOB               DATE ,
    DOD               DATE ,
    GenderID          INTEGER ,
    PrimaryPostalCode VARCHAR2 (10 CHAR) ,
    ChangeNumber      INTEGER DEFAULT 0 ,
    dateCreated       DATE NOT NULL ,
    lastUpdated       DATE ,
    ArchivedOn        DATE
  ) ;
ALTER TABLE Person ADD CONSTRAINT Person_PK PRIMARY KEY ( ID ) ;

CREATE
  TABLE Phone
  (
    ID          INTEGER NOT NULL ,
    PersonID    INTEGER NOT NULL ,
    PhoneType   VARCHAR2 (20 CHAR) ,
    "Number"    VARCHAR2 (20 CHAR) NOT NULL ,
    IsActive    CHAR (1) ,
    dateCreated DATE NOT NULL ,
    lastUpdated DATE ,
    ArchivecOn  DATE
  ) ;
ALTER TABLE Phone ADD CONSTRAINT phone_PK PRIMARY KEY ( ID ) ;

CREATE
  TABLE Qrcode
  (
    ID               INTEGER NOT NULL ,
    subjectID        INTEGER NOT NULL ,
    startDate        DATE ,
    startDateTS      INTEGER ,
    expirationDate   DATE ,
    expirationDateTS INTEGER ,
    token CLOB ,
    image BLOB ,
    dateCreated DATE NOT NULL ,
    ArchivedOn  DATE
  ) ;
ALTER TABLE Qrcode ADD CONSTRAINT Qrcode_PK PRIMARY KEY ( ID ) ;

CREATE
  TABLE Subject
  (
    ID           INTEGER NOT NULL ,
    PersonID     INTEGER NOT NULL ,
    SSSID        VARCHAR2 (255 CHAR) NOT NULL ,
    GUID         CHAR (36 CHAR) ,
    ChangeNumber INTEGER DEFAULT 0 ,
    dateCreated  DATE NOT NULL ,
    lastUpdated  DATE ,
    ArchivedOn   DATE
  ) ;
ALTER TABLE Subject ADD CONSTRAINT Subject_PK PRIMARY KEY ( ID ) ;
ALTER TABLE Subject ADD CONSTRAINT SubjectId__UQ UNIQUE ( SSSID ) ;

ALTER TABLE Address ADD CONSTRAINT Address_ContactData_FK FOREIGN KEY (
PersonID ) REFERENCES ContactData ( PersonID ) ;

ALTER TABLE ContactData ADD CONSTRAINT ContactData_Person_FK FOREIGN KEY (
PersonID ) REFERENCES Person ( ID ) ;

ALTER TABLE DemographicData ADD CONSTRAINT DemographicData_Person_FK FOREIGN
KEY ( PersonID ) REFERENCES Person ( ID ) ;

ALTER TABLE Email ADD CONSTRAINT Email_ContactData_FK FOREIGN KEY ( PersonID )
REFERENCES ContactData ( PersonID ) ;

ALTER TABLE Person ADD CONSTRAINT Person_Gender_FK FOREIGN KEY ( GenderID )
REFERENCES Gender ( ID ) ;

ALTER TABLE Phone ADD CONSTRAINT Phone_ContactData_FK FOREIGN KEY ( PersonID )
REFERENCES ContactData ( PersonID ) ;

ALTER TABLE Qrcode ADD CONSTRAINT Qrcode_Subject_FK FOREIGN KEY ( subjectID )
REFERENCES Subject ( ID ) ;

ALTER TABLE Subject ADD CONSTRAINT Subject_Person_FK FOREIGN KEY ( PersonID )
REFERENCES Person ( ID ) ;


-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                             9
-- CREATE INDEX                             0
-- ALTER TABLE                             18
-- CREATE VIEW                              0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
