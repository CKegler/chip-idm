package edu.harvard.childrens.idm

import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Person, Gender])
class PersonSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test valid postalCode"() {

        given:
        mockForConstraintsTests Person

        when: 'The postal code has 3 digits, < 5 digits required'
        def testPersonA = new Person(id: 999)
        testPersonA.lastName = "TestLast2"
        testPersonA.firstName = "TestFirst2"
        testPersonA.dob = new Date(90, 1, 1)
        testPersonA.gender = Gender.findByName("Male")
        testPersonA.dateCreated = new Date()
        testPersonA.primaryPostalCode="1234"

        then: 'validation on 4-digit zip should fail'
        !testPersonA.validate()

        when: 'The postal code has  5 digits required'
        testPersonA.primaryPostalCode="12345"

        then: 'validation on 5-digit zip should pass'
        testPersonA.validate()
    }
}
