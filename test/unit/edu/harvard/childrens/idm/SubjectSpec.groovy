package edu.harvard.childrens.idm

import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Subject,Person, Gender])
class SubjectSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test subjectid uniqueness"() {
        given:
        mockForConstraintsTests Subject
        
        when: 'Establish 2 valid people who will later become subjects'
        def testPersonA = new Person(id: 999)
        testPersonA.lastName = "TestLast2"
        testPersonA.firstName = "TestFirst2"
        testPersonA.dob = new Date(90, 1, 1)
        testPersonA.gender = Gender.findByName("Male")
        testPersonA.dateCreated = new Date()
        testPersonA.primaryPostalCode="12345"
        testPersonA.save()

        def testPersonB = new Person(id:998)
        testPersonB.lastName = "TestLast3"
        testPersonB.firstName = "TestFirst3"
        testPersonB.dob = new Date(90, 1, 1)
        testPersonB.gender = Gender.findByName("Female")
        testPersonB.dateCreated = new Date()
        testPersonB.primaryPostalCode="12345"
        
        then: 'validation on people should pass'
        testPersonA.validate()
        testPersonB.validate()
        testPersonA.save()
        testPersonB.save()
     
        when: 'First Subject is established with SSSID=Subj001'
        def subjectA = new Subject(id: 10, person:testPersonA)
        subjectA.dateCreated = new Date()
        subjectA.sssid="Subj001"

        then: 'First Subject should be successfully validated'
        subjectA.validate()
        subjectA.save()

        when: 'Second Subject is established with existing SSSID=Subj001'
        def subjectB = new Subject(id: 20, person:testPersonB)
        subjectB.dateCreated = new Date()
        subjectB.sssid="Subj001"

        then: 'Second subject should not validate b/c of uniqueness conflict with SSSID'
        subjectB.validate()
        subjectB.save()
    }
}
